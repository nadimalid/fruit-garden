<?php

class Garden {
    private array $trees = [];
    private array $harvest = [];

    public function get_trees(): array {
        return $this->trees;
    }

    public function add_tree(Tree $tree) {
        array_push($this->trees, $tree);
    }

    public function harvest_garden() {
        
        foreach($this->trees as $tree) {
            $type = $tree->get_type();
            
            if(!array_key_exists($type, $this->harvest)){
                $this->harvest[$type] = ["type" => $type,"amount" => $tree->get_fruits_amount(), "weight" => $tree->get_total_weight()];
            } else if(array_key_exists($type, $this->harvest)){
                $this->harvest[$type]["amount"] += $tree->get_fruits_amount();
                $this->harvest[$type]["weight"] += $tree->get_total_weight();
            }
        }
    }

    public function get_harvest(): array {
        return $this->harvest;
    }

    public function gr_to_kg(int $weight): float {
        return ($weight / 1000);
    }
}