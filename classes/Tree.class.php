<?php
abstract class Tree {
	
	public function __construct(protected int $id = 0, 
                                protected string $type = "", 
                                protected int $fruits_amount = 0, 
                                protected array $fruits_weight = [], 
                                protected int $total_weight = 0) {
		
	}
	
	public function get_id(): int {
		return $this->id;
	}
	
	public function set_id(int $id) {
		$this->id = $id;
	}

    public function get_type(): string {
		return $this->type;
	}
	
	public function set_type(string $type) {
		$this->type = $type;
	}
	
	public function get_fruits_amount(): int {
		return $this->fruits_amount;
	}
	
	public function set_fruits_amount(int $amount) {
		$this->fruits_amount = $amount;
	}
	
	public function get_fruits_weight(): array {
		return $this->fruits_weight;
	}
	
	public function set_fruits_weight() {
		
	}
	
	public function set_total_weight() {
        $this->total_weight = array_sum($this->fruits_weight);
	}

    public function get_total_weight(): int {
        return $this->total_weight;
    }
}