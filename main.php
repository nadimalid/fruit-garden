<?php
require_once("./classes/Tree.class.php");
require_once("./classes/Apple.class.php");
require_once("./classes/Pear.class.php");
require_once("./classes/Garden.class.php");

$garden = new Garden();

for($i = 1; $i <= 10;$i++) {
    $tree_id =  count($garden->get_trees()) + 1;
    $apple = new Apple($id = $tree_id, $type = "apple", $fruits_amount = rand(40,50));
    $apple->set_fruits_weight();
    $apple->set_total_weight();

    $garden->add_tree($apple);

    unset($apple);
}

for($i = 1; $i <= 15;$i++) {
    $tree_id =  count($garden->get_trees()) + 1;
    $pear = new Pear($id = $tree_id, $type = "pear", $fruits_amount = rand(0,20));
    $pear->set_fruits_weight();
    $pear->set_total_weight();

    $garden->add_tree($pear);

    unset($pear);
}

$garden->harvest_garden();

$harvest = $garden->get_harvest();

echo <<<EOT
Oбщее количество собранных яблок {$harvest["apple"]["amount"]} с весом {$harvest["apple"]["weight"]} гр ({$garden->gr_to_kg($harvest["apple"]["weight"])} кг)
Oбщее количество собранных груш  {$harvest["pear"]["amount"]} с весом {$harvest["pear"]["weight"]} гр ({$garden->gr_to_kg($harvest["pear"]["weight"])} кг)
EOT;