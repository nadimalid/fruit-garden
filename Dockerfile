FROM php:8.1-apache

COPY ./ /var/www/html/

CMD ["php", "main.php"]