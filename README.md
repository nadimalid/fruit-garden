# fruit-garden


## Running using Docker

### From an image

build the image

```shell
docker image build -t fruit-garden .
```

then run

```shell
docker container run --rm --name garden -it fruit-garden
```

or 

### With a single PHP script

```shell
docker run -it --rm --name fruit-garden -v ${pwd}:/usr/src/garden -w /usr/src/garden php:8.1-cli php main.php
```